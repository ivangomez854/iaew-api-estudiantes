import {AppRoutes} from './routes';
import * as dotenv from 'dotenv';
const bodyParser = require('body-parser');

const express = require('express');
const app = express();
const { auth } = require('express-oauth2-jwt-bearer');
// configuro las variables de entorno
dotenv.config();

const port = process.env.PORT || 3000;
app.use(bodyParser.json());

const jwtCheck = auth({
    audience: 'https://api.example.com/estudiantes',
    issuerBaseURL: 'https://dev-ktiy20mxl6ragjzp.us.auth0.com/',
    tokenSigningAlg: 'RS256'
});

// enforce on all endpoints
app.use(jwtCheck);

// Registramos todas las rutas de aplicacion
AppRoutes.forEach((route) => {
    app.use(route.path, route.action);
});

app.listen(port, () => {
    console.log(`Servidor escuchando en el puerto ${port}`);
});