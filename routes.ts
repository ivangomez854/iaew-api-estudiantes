import {EstudiantesController} from "./src/controller/ControllerEstudiantes";

export const AppRoutes = [
    {
        path: '/estudiantes/obtener-estudiantes',
        method: 'get',
        action: EstudiantesController.obtenerEstudiantes
    },
    {
        path: '/estudiantes/obtener-estudiante-por-id/:id',
        method: 'get',
        action: EstudiantesController.obtenerEstudiantePorId
    },
    {
        path: '/estudiantes/nuevo-estudiante',
        method: 'post',
        action: EstudiantesController.nuevoEstudiante
    },
    {
        path: '/estudiantes/actualizar-estudiante/:id',
        method: 'put',
        action: EstudiantesController.actualizarEstudiante
    },
    {
        path: '/estudiantes/eliminar-estudiante/:id',
        method: 'delete',
        action: EstudiantesController.eliminarEstudiante
    },
];